﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CreateSpreadSheet.Calculate;

namespace CreateSpreadSheet
{
    public class CommandFactory
    {
        public string[] cmd;


        public CommandFactory(string[] command)
        {
            cmd = command;
        }

        public ICommand GetCommand(string commandOption)
        {
            switch (commandOption.ToLower())
            {
                case "c":
                    ArrayHelper.sheetArray = new char[Convert.ToInt32(cmd[2]) + ArrayHelper.startIndex + 1, Convert.ToInt32(cmd[1]) + 1];
                    return new CreateSpreadSheetCommand(Convert.ToInt32(cmd[1]), Convert.ToInt32(cmd[2]));
                case "n":
                    return new InsertNumberCommand(Convert.ToInt32(cmd[1]), Convert.ToInt32(cmd[2]), Convert.ToInt32(cmd[3]));
                
                case "s":
                    return new CalculateSumCommand(Convert.ToInt32(cmd[1]), Convert.ToInt32(cmd[2]), Convert.ToInt32(cmd[3]), Convert.ToInt32(cmd[4]), Convert.ToInt32(cmd[5]), Convert.ToInt32(cmd[6]));

                default:
                    throw new InvalidOperationException();
            }
        }
    }
}
