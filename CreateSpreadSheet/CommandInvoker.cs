﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateSpreadSheet
{
    public class CommandInvoker: ICommandInvoker
    {

        public void Draw(string[] cord)
        {
            try
            {
                ICommand command = new CommandFactory(cord).GetCommand(cord[0]);
                command.Execute();
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine("Invalid Operation");
                throw ex;
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine("Please select range in the sheet");
                throw ex;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Bad Operation");
                throw ex;
            }

        }
    }
}
