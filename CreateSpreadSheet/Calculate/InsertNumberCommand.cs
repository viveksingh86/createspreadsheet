﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CreateSpreadSheet.Calculate
{
    public class InsertNumberCommand : ICommand
    {

        public InsertNumberCommand(int startX, int startY, int num)
        {
            this.startX = startX;
            this.startY = startY;
            this.num = num;
        }

        public int startX { get; set; }

        public int startY { get; set; }

        public int num { get; set; }

        public void Execute()
        {
            if (ArrayHelper.sheetArray == null )
               throw new Exception("Sheet Not Created");
            startY = startY + ArrayHelper.startIndex - 1;

            ArrayHelper.writeonconsole(startX, startY, num);

        }
    }


}
