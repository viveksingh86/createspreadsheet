﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateSpreadSheet
{


    public class CreateSpreadSheetCommand : ICommand
    {

        public CreateSpreadSheetCommand(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public int height { get; set; }

        public int width { get; set; }


        public void Execute()
        {
            int startY = ArrayHelper.startIndex;

            for (int i = 0; i < width + 1; i++)
            {
                ArrayHelper.writeonconsole(i, startY - 1, '-');
                ArrayHelper.writeonconsole(i, startY + height, '-');
            }

            for (int i = startY; i < height + startY; i++)
            {
                ArrayHelper.writeonconsole(0, i, '|');
                ArrayHelper.writeonconsole(width, i, '|');
            }
        }
    }
    
}
