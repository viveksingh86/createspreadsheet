﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateSpreadSheet.Calculate
{
    public class CalculateSumCommand : ICommand
    {

        public CalculateSumCommand(int startX, int startY, int endX, int endY, int sumX, int sumY)
        {
            this.startX = startX;
            this.startY = startY;
            this.endX = endX;
            this.endY = endY;
            this.sumX = sumX;
            this.sumY = sumY;
        }

        public int startX { get; set; }

        public int startY { get; set; }

        public int endX { get; set; }

        public int endY { get; set; }

        public int sumX { get; set; }

        public int sumY { get; set; }


        public void Execute()
        {
            if (ArrayHelper.sheetArray == null)
                throw new Exception("Sheet Not Created");
            int startIndexY = ArrayHelper.startIndex - 1;

            startY = startY + startIndexY;
            endY = endY + startIndexY;
            sumY = sumY + startIndexY;
            int sum = 0;
            for (int i = startY; i <= endY; i++)
            {
                for (int j = startX; j <= endX; j++)
                {
                    sum = sum + (int)ArrayHelper.sheetArray[i, j];
                }

            }

            ArrayHelper.writeonconsole(sumX, sumY, sum);

        }
    }

}
