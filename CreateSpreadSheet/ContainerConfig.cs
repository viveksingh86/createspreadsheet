﻿using System.Reflection;
using System.Linq;
using Autofac;


namespace CreateSpreadSheet
{
    public static class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>().As<IApplication>();
            builder.RegisterType<CommandInvoker>().As<ICommandInvoker>();
            return builder.Build();

        }

    }
}