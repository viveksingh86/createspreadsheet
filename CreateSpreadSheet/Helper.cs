﻿using System;

namespace CreateSpreadSheet
{
    public static class ArrayHelper
    {

        public static char[,] sheetArray { get; set; }

        public static int startIndex { get; set; }

        public static void writeonconsole(int x, int y, char ch)
        {
            Console.SetCursorPosition(x, y);
            sheetArray[y, x] = ch;
            Console.Write(ch);
        }

        public static void writeonconsole(int x, int y, int ch)
        {
            Console.SetCursorPosition(x, y);
            sheetArray[y, x] = (char)ch;
            Console.Write(ch);
        }

    }
}
