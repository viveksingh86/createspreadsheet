﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace CreateSpreadSheet
{
    public class Application: IApplication
    {
        ICommandInvoker _commandInvoker;
        public Application(ICommandInvoker commandInvoker)
        {
            _commandInvoker = commandInvoker;
        }
        public void Run()
        {
            
            Console.WriteLine("Draw Canvas for help press --h or input the command");
            Boolean quit = false;
            int initialCursor;
            int initialCursorTop;
            ArrayHelper.startIndex = 20;
            while (!quit)
            {
                var command = Console.ReadLine();
                initialCursor = Console.CursorLeft;
                initialCursorTop = Console.CursorTop;
                var cmd = command.Split(' ');
                if (cmd[0].ToLower() == "--h" || cmd[0].ToLower() == "--help")
                {
                    
                    Console.WriteLine(getHelp());
                }
                else if (cmd[0].ToLower() == "q")
                {
                    quit = true;
                }
                else
                {
                    try
                    {
                        _commandInvoker.Draw(cmd);
                    }
                    catch (Exception e)
                    {
                        Console.SetCursorPosition(initialCursor, Console.CursorTop);
                        continue;
                    }
                   

                }
                Console.SetCursorPosition(initialCursor, initialCursorTop);
            }
           
        }

        public string getHelp()
        {
            string helpText = "C w h           Should create a new spread sheet of width w and height h (i.e. the spreadsheet can hold w * h amount of cells).\n\nN x1 y1 v1 Should insert a number in specified cell (x1,y1) \n\nS x1 y1 x2 y2 x3 y3 Should perform sum on top of all cells from x1 y1 to x2 y2 and store the result in x3 y3\n\n\nQ Should quit the program.";
            return helpText;
        }

    }
}
