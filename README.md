## INSTRUCTIONS TO USE 
	
	1.	Clone the Repository and open in Visual Studio	
	2.  Build the application
	3.  Run the app and enter below command
		 
		Command	Description	   
		C w h	Should create a new spread sheet of width w and height h (i.e. the spreadsheet can hold w * h amount of cells).	   
		N x1 y1 v1	Should insert a number in specified cell (x1,y1)	   
		S x1 y1 x2 y2 x3 y3	Should perform sum on top of all cells from x1 y1 to x2 y2 and store the result in x3 y3	   
		Q	Should quit the program.	
		
	4. In visual Studio Select Test->Run->All Test

 