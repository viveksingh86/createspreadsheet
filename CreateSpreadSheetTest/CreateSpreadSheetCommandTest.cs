﻿using System;
using CreateSpreadSheet;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace CreateSpreadSheetTest
{
    [TestClass]
    public class CreateSpreadSheetCommandTest
    {
        string[] cmd = { "C", "20", "4" };

       [TestMethod]
        public void Test_CreateCanvas()
        {
            ArrayHelper.startIndex = 20;
            CommandInvoker invoker = new CommandInvoker();
            invoker.Draw(cmd);
            Assert.AreEqual(ArrayHelper.sheetArray[19, 0], '-');
            Assert.AreEqual(ArrayHelper.sheetArray[20, 20], '|');
            Assert.AreEqual(ArrayHelper.sheetArray[24, 20], '-');
            ArrayHelper.sheetArray = null;
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Test_InvalidOperationDoesntWork()
        {
            string[] cmd = { "Z", "1", "2", "2" };
            ArrayHelper.startIndex = 20;
            CommandInvoker invoker = new CommandInvoker();
            invoker.Draw(cmd);
        }

    }
}
