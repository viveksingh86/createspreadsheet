﻿using System;
using CreateSpreadSheet;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace CreateSpreadSheetTest
{
    [TestClass]
    public class CalculateSumCommandTest
    {


        [TestMethod]
        [ExpectedException(typeof(Exception), "Sheet Not Created")]
        public void Test_CalculateSumDoesntWork()
        {
            string[] cmd = { "S", "1", "2", "1", "3", "1", "4" };
            ArrayHelper.startIndex = 20;
            CommandInvoker invoker = new CommandInvoker();
            invoker.Draw(cmd);
        }

        [TestMethod]
        public void Test_CalculateSumDoesWork()
        {
            CommandInvoker invoker = new CommandInvoker();
            ArrayHelper.startIndex = 20;
            string[] cmd = { "C", "20", "4" };
            invoker.Draw(cmd);
            string[] insertCmd = { "N", "1", "2", "2" };
            invoker.Draw(insertCmd);
            string[] insertCmd2 = { "N", "1", "3", "4" };
            invoker.Draw(insertCmd2);
            string[] sumCmd = { "S", "1", "2", "1", "3", "1", "4" };
            invoker.Draw(sumCmd);
            Assert.AreEqual((int)ArrayHelper.sheetArray[23, 1], 6);
            ArrayHelper.sheetArray = null;
        }

    }
}
