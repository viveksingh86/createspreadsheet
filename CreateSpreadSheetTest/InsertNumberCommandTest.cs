﻿using System;
using CreateSpreadSheet;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace CreateSpreadSheetTest
{
    [TestClass]
    public class InsertNumberCommandTest
    {
        

        [TestMethod]
        [ExpectedException(typeof(Exception), "Sheet Not Created")]
        public void Test_CreateNumberDoesntWork()
        {
            string[] cmd = { "N", "1", "2", "2" };
            ArrayHelper.startIndex = 20;
            CommandInvoker invoker = new CommandInvoker();
            invoker.Draw(cmd);
        }

        [TestMethod]
        public void Test_CreateNumberDoesWork()
        {
            CommandInvoker invoker = new CommandInvoker();
            ArrayHelper.startIndex = 20;
            string[] cmd = { "C", "20", "4" };
            invoker.Draw(cmd);
            string[] insertCmd = { "N", "1", "2", "2" };
            invoker.Draw(insertCmd);
            Assert.AreEqual((int)ArrayHelper.sheetArray[21, 1], 2);
            ArrayHelper.sheetArray = null;
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void Test_IndexOutOfRangeException()
        {
            CommandInvoker invoker = new CommandInvoker();
            ArrayHelper.startIndex = 20;
            string[] cmd = { "C", "20", "4" };
            invoker.Draw(cmd);
            string[] insertCmd = { "N", "22", "3", "2" };
            invoker.Draw(insertCmd);
        }
    }
}
